from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from .models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.assignee = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    return render(request, "tasks/create_task.html", {"form": form})


@login_required
def show_my_task(request):
    task = Task.objects.filter(assignee=request.user)
    return render(request, "tasks/show_my_task.html", {"task": task})
